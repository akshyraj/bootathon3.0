# Heading 1
### Heading 3
*This line is italic*

_This line is italic_

**This line is italic**<br>
__This line is italic__

> Blockquues
>> Blockquets

1. The list start with one
1. Watch out for number 2


- Hi
+ si
* hi

Example : [Link text](link "Title")

<akshayrajsinh@gmail.com>

Disable link  `Google.com`

|Name|Roll no|class|
|----|----|----|
|Akshayaj|122|CE|
|Akshayaj|122|CE|

|Name|Roll no|class|
|:----|----:|:----:|
|Akshayaj|122|CE|
|Akshayaj|122|CE|

![image](../Introduction/nebula-2560x1600-galaxy-stars-planets-6348.jpg "AJ")
