function cal()
{
    let t1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");          // Take the input number
    var table:HTMLTableElement = <HTMLTableElement>document.getElementById("tab");       // Table for the table
    var a:number = +t1.value;
    var c:number = 1;
    while(c<=a)     // loop for create the talbe
    {
        var row:HTMLTableRowElement = table.insertRow();    // insert the row in tablr
        var cell:HTMLTableDataCellElement = row.insertCell();   // insert cell in that row
        var text:string;
        cell.align = "center";  
        cell.width = '100px';
        text = a.toString();    
        cell.append(text);  // put the value in cell

        var cell:HTMLTableDataCellElement = row.insertCell();
        var text:string;
        cell.align = "center";
        cell.width = '100px';
        text = c.toString();
        cell.append(text);

        var cell:HTMLTableDataCellElement = row.insertCell();
        var text:string;
        cell.align = "center";
        cell.width = '100px';
        text = (c*a).toString();
        cell.append(text);
        c++;
    }
}