function forloop() {
    var count;
    for (count = 1; count <= 100; count++) {
        console.log(count);
    }
}
function whileloop() {
    var count = +100;
    while (count >= 1) {
        console.log(count);
        count--;
    }
}
function dowhileloop() {
    var count = 0;
    do {
        console.log(count);
        count++;
    } while (count < 100);
}
function fact() {
    var a = document.getElementById("t1");
    var x = +a.value;
    var ans = 1;
    while (x != 0) {
        ans = ans * x;
        x--;
    }
    document.getElementById("ans").innerHTML = ans.toString();
}
function dynamic() {
    var a = document.getElementById("t2");
    var table = document.getElementById("tab");
    var count = 1;
    var num = +a.value;
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    for (count = 1; count <= num; count++) {
        var row = table.insertRow();
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);
        var cell2 = row.insertCell();
        var text2 = document.createElement("input");
        text2.type = "text";
        text2.style.textAlign = "center";
        text2.value = (count * count).toString();
        cell2.appendChild(text);
    }
}
function dynamic2() {
    var a = document.getElementById("t3");
    var table = document.getElementById("tab2");
    var count = 1;
    var num = +a.value;
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    for (count = 1; count <= num; count++) {
        var row = table.insertRow();
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.id = "t" + count;
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.id = "tt" + count;
        text.style.textAlign = "center";
        cell.appendChild(text);
    }
}
function square() {
    var table = document.getElementById("tab2");
    var count = 1;
    while (count < table.rows.length) {
        var row_num = document.getElementById("t" + count);
        var num = +row_num.value;
        var square_num = document.getElementById("tt" + count);
        square_num.value = (num * num).toString();
    }
}
//# sourceMappingURL=loop.js.map