function forloop()
{
    var count:number;
    for( count=1;count<=100;count++)
    {
        console.log(count);
    }
}
function whileloop()
{
    var count:number = +100;
    while(count>=1)
    {
        console.log(count);
        count--;
    }
}
function dowhileloop()
{
    var count:number = 0;
    do{
        console.log(count);
        count++;
    }while(count<100)
}
function fact()
{
    var a:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var x:number = +a.value;
    var ans:number=1;
    while(x!=0)
    {
        ans= ans * x;
        x--;
    }
    document.getElementById("ans").innerHTML = ans.toString();

}
function dynamic(){
    var a:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var table:HTMLTableElement = <HTMLTableElement>document.getElementById("tab");
    var count:number = 1;
    var num:number = +a.value;
    while(table.rows.length > 1)
    {
        table.deleteRow(1);
    }
    for(count=1;count<=num;count++)
    {
        var row:HTMLTableRowElement = table.insertRow();
        var cell:HTMLTableDataCellElement = row.insertCell();
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);

        var cell2:HTMLTableDataCellElement = row.insertCell();
        var text2:HTMLInputElement = document.createElement("input");
        text2.type = "text";
        text2.style.textAlign = "center";
        text2.value = (count*count).toString();
        cell2.appendChild(text);

    }
}

function dynamic2(){
    var a:HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
    var table:HTMLTableElement = <HTMLTableElement>document.getElementById("tab2");
    var count:number = 1;
    var num:number = +a.value;
    while(table.rows.length > 1)
    {
        table.deleteRow(1);
    }
    for(count=1;count<=num;count++)
    {
        var row:HTMLTableRowElement = table.insertRow();
        var cell:HTMLTableDataCellElement = row.insertCell();
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "t"+count;
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);

        var cell:HTMLTableDataCellElement = row.insertCell();
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "tt"+count;
        text.style.textAlign = "center";
        cell.appendChild(text);
    }
}
function square()
{
    var table:HTMLTableElement = <HTMLTableElement>document.getElementById("tab2");
    var count:number = 1;
    while(count < table.rows.length)
    {
        var row_num:HTMLInputElement = <HTMLInputElement>document.getElementById("t"+count);
        var num:number = +row_num.value;
        var square_num:HTMLInputElement = <HTMLInputElement>document.getElementById("tt"+count);
        square_num.value = (num*num).toString();

    }
}