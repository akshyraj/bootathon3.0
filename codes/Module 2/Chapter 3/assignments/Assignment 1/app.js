function check() {
    let t1 = document.getElementById("t1"); // Take the input number
    var p = document.getElementById("p"); // P tag for output
    var a = +t1.value; // Create new variable for store the value of input
    if (isNaN(a)) // Check that input is number  
     {
        alert("Given input is not a number"); // If not number then alert the user  
    }
    else { // Check for the number is odd or even
        if (a % 2 == 0) {
            p.innerHTML = "Number is Even";
        }
        else {
            p.innerHTML = "Number is odd";
        }
    }
}
//# sourceMappingURL=app.js.map