function check(){
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");          // Take the input number
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");    // P tag for output
    var a:number = +t1.value;           // Create new variable for store the value of input
    if(isNaN(a))                        // Check that input is number  
    {                                           
        alert("Given input is not a number");   // If not number then alert the user  
    }
    else
    {                                           // Check for the number is odd or even
        if(a%2==0)
        {
            p.innerHTML = "Number is Even";     
        }
        else
        {
            p.innerHTML = "Number is odd";
        }
    }
}