function check1()
{
    var a:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");   // get the input
    var b:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var c:HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
    var data:string = a.value;          // Store in the variable
    var real:number;                    // variable for the output
    var imaginary:number;
    var i=data.indexOf("+");            // Take the index for the split the string
    var j=data.lastIndexOf("-");        // Take the last index for the negative complex part for split the string
    if(i!=-1)                           // Check for the '+' sign is there in string
    {
        real = +data.substring(0,i);    // Split the string from the '+' signt
        imaginary = +data.substring(i+1,data.length-1);
        if(data.charAt(0)=='-')         // Check for the real part is negative or not
        {   
            b.value = "Real Part : " + real + " And it is negative";
        }
        else
        {
            b.value = "Real Part : " + real + " And it is positive";
        }
        c.value = "complex Part :" + imaginary + " And it is positive";
    }
    else if(j!=-1)                      // Check for '-' sign in the string
    {   
        if(j!=0)                        // Check for the negative complex part
        {
        real = +data.substring(0,j);
        imaginary = +data.substring(j+1,data.length-1);
        if(data.charAt(0)=='-')         // Check for real part is negative or positive 
        {
            b.value = "Real Part : " + real + " And it is negative";
        }
        else
        {
            b.value = "Real Part : " + real + " And it is positive";
        }
        c.value = "complex Part :" + imaginary + " And it is negative";
    }
    }
    else{       // check for complex part is there or not
        real = +data;
        if(data.charAt(0)=='-')     // Check for real part is negative or positive
        {
            b.value = "Real Part : " + real + "And it is negative";
        }
        else
        {
            b.value = "Real Part : " + real + "And it is positive";
        }
        c.value = "complex Part : 0";
    }
}