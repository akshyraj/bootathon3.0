function area1()
{
    let t11:HTMLInputElement = <HTMLInputElement>document.getElementById("t11");    // Get the input from html
    let t12:HTMLInputElement = <HTMLInputElement>document.getElementById("t12");
    let t21:HTMLInputElement = <HTMLInputElement>document.getElementById("t21");
    let t22:HTMLInputElement = <HTMLInputElement>document.getElementById("t22");
    let t31:HTMLInputElement = <HTMLInputElement>document.getElementById("t31");
    let t32:HTMLInputElement = <HTMLInputElement>document.getElementById("t32");
    let t41:HTMLInputElement = <HTMLInputElement>document.getElementById("t41");
    let t42:HTMLInputElement = <HTMLInputElement>document.getElementById("t42");
    let ans:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");
    var x1:number = parseFloat(t11.value);  //Store it in the variables
    var y1:number = parseFloat(t12.value);
    var x2:number = parseFloat(t21.value);
    var y2:number = parseFloat(t22.value);
    var x3:number = parseFloat(t31.value);
    var y3:number = parseFloat(t32.value);
    var x:number = parseFloat(t41.value);
    var y:number = parseFloat(t42.value);
    var ABC:number = (x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2))/2;  // Calulate area of the triangles
    var PAB:number = (x*(y1-y2) + x1*(y2-y) + x2*(y-y1))/2;
    var PBC:number = (x*(y2-y3) + x2*(y3-y) + x3*(y-y2))/2;
    var PAC:number = (x*(y3-y1) + x3*(y1-y) + x1*(y-y3))/2;
    var sum:number = PAB + PBC + PAC;                           // Sum the are of the triangles
    if(Math.abs(ABC-sum)<0.0000001)                             // Check for the point is in the triangle or not
    {
        alert("Point is in the trianlge");
    }
    else
    {
        alert("Point is not in triangle");
    }
}