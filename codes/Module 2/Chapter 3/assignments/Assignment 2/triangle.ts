function check()
{
    var x:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");   //Take the three sides value    
    var y:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var z:HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +x.value;        // Store the value in the variable
    var b:number = +y.value;
    var c:number = +z.value;
    if( a==b && b==c)               // Check for triangle is equilateral
    {
        p.innerHTML = "The triangle is equilateral";
    }
    else if((a==b && a!=c) || (b==c && b!=a) || (a==c && a!=b))   // Check for triangle is isosceles
    {
        p.innerHTML = "The triangle is isosceles";
        if( (Math.pow(a,2)== Math.pow(b,2) + Math.pow(c,2) ) || (Math.pow(b,2)== Math.pow(a,2) + Math.pow(c,2)) || Math.pow(c,2)== Math.pow(b,2) + Math.pow(a,2)) // Also check for tight angle triangle
        {
            p.innerHTML += " and also The triangle is right angle";
        }
    }
    else if(a!=b && b!=c && a!=c)   // Check for triangle is scalence
    {   
        p.innerHTML = "The triangle is scalence";
        if( (Math.pow(a,2)== Math.pow(b,2) + Math.pow(c,2) ) || (Math.pow(b,2)== Math.pow(a,2) + Math.pow(c,2)) || Math.pow(c,2)== Math.pow(b,2) + Math.pow(a,2))
        {
            p.innerHTML += " and also The triangle is right angle";     // Also check for triangle is right angle
        }
    }

}