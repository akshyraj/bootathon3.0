function check() {
    var x = document.getElementById("t1"); //Take the three sides value    
    var y = document.getElementById("t2");
    var z = document.getElementById("t3");
    var p = document.getElementById("p");
    var a = +x.value; // Store the value in the variable
    var b = +y.value;
    var c = +z.value;
    if (a == b && b == c) // Check for triangle is equilateral
     {
        p.innerHTML = "The triangle is equilateral";
    }
    else if ((a == b && a != c) || (b == c && b != a) || (a == c && a != b)) // Check for triangle is isosceles
     {
        p.innerHTML = "The triangle is isosceles";
        if ((Math.pow(a, 2) == Math.pow(b, 2) + Math.pow(c, 2)) || (Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == Math.pow(b, 2) + Math.pow(a, 2)) // Also check for tight angle triangle
         {
            p.innerHTML += " and also The triangle is right angle";
        }
    }
    else if (a != b && b != c && a != c) // Check for triangle is scalence
     {
        p.innerHTML = "The triangle is scalence";
        if ((Math.pow(a, 2) == Math.pow(b, 2) + Math.pow(c, 2)) || (Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == Math.pow(b, 2) + Math.pow(a, 2)) {
            p.innerHTML += " and also The triangle is right angle"; // Also check for triangle is right angle
        }
    }
}
//# sourceMappingURL=triangle.js.map