// Scientific Calculator for perform addition, subtraction, multiplication, division,
// sin, cos, tan, square root and power.
function add() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var p = document.getElementById("p");
    var a = +t1.value;
    var b = +t2.value;
    if (isNaN(a)) // Check for first input
     {
        alert("First input is not a number");
    }
    else if (isNaN(b)) // Check for second input
     {
        alert("Second input is not a number");
    }
    else {
        var ans = a + b; // Add to numbers
        p.innerHTML = "Addition is " + ans.toString(); // Print the ans
    }
}
function sub() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var p = document.getElementById("p");
    var a = +t1.value;
    var b = +t2.value;
    if (isNaN(a)) {
        alert("First input is not a number");
    }
    else if (isNaN(b)) {
        alert("Second input is not a number");
    }
    else {
        var ans = a - b;
        p.innerHTML += "<br>Substraction is " + ans.toString();
    }
}
function mul() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var p = document.getElementById("p");
    var a = +t1.value;
    var b = +t2.value;
    if (isNaN(a)) {
        alert("First input is not a number");
    }
    else if (isNaN(b)) {
        alert("Second input is not a number");
    }
    else {
        var ans = a * b;
        p.innerHTML += "<br>Multiplivcation is " + ans.toString();
    }
}
function div() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var p = document.getElementById("p");
    var a = +t1.value;
    var b = +t2.value;
    if (isNaN(a)) {
        alert("First input is not a number");
    }
    else if (isNaN(b)) {
        alert("Second input is not a number");
    }
    else if (b == 0) {
        alert("Not divide by zero");
    }
    else {
        var ans = a / b;
        p.innerHTML += "<br>Division is " + ans.toString();
    }
}
function sin1() {
    let t1 = document.getElementById("t1");
    var p = document.getElementById("p");
    if (isNaN(+t1.value)) {
        alert("Input is not a number");
    }
    else {
        var a = Math.PI / 180 * parseFloat(t1.value); // Convert value into radian
        var ans = Math.sin(a);
        p.innerHTML += "<br>Sin of input is" + ans.toString();
    }
}
function cos1() {
    let t1 = document.getElementById("t1");
    var p = document.getElementById("p");
    if (isNaN(+t1.value)) {
        alert("First Input is not a number");
    }
    else {
        var a = Math.PI / 180 * parseFloat(t1.value);
        var ans = Math.cos(a);
        p.innerHTML += "<br>Cos of input is" + ans.toString();
    }
}
function tan1() {
    let t1 = document.getElementById("t1");
    var p = document.getElementById("p");
    if (isNaN(+t1.value)) {
        alert("Input is not a number");
    }
    else if (+t1.value == 90) // Handle exception case for tan90 that is infinite
     {
        alert("Tan90 is undefinded");
    }
    else {
        var a = Math.PI / 180 * parseFloat(t1.value);
        var ans = Math.tan(a);
        p.innerHTML += "<br>Tan of input is" + ans.toString();
    }
}
function sroot() {
    let t1 = document.getElementById("t1");
    var p = document.getElementById("p");
    var a = +t1.value;
    if (isNaN(a)) {
        alert("Input is not a number");
    }
    else {
        var ans = Math.sqrt(a);
        p.innerHTML += "<br>Square root of input is" + ans.toString();
    }
}
function power() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var p = document.getElementById("p");
    var a = +t1.value;
    var b = +t2.value;
    if (isNaN(a)) {
        alert("First input is not a number");
    }
    else if (isNaN(b)) {
        alert("Second input is not a number");
    }
    else {
        var ans = Math.pow(a, b);
        p.innerHTML += "<br>Ans of Power is" + ans.toString();
    }
}
//# sourceMappingURL=calc.js.map