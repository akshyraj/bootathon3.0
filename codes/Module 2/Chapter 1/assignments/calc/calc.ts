// Scientific Calculator for perform addition, subtraction, multiplication, division,
// sin, cos, tan, square root and power.

function add(){                     //  Function for addition
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +t1.value;
    var b:number = +t2.value;
    if(isNaN(a))                    // Check for first input
    {
        alert("First input is not a number");
    }   
    else if(isNaN(b))               // Check for second input
    {
        alert("Second input is not a number");
    }
    else{
        var ans:number = a + b;     // Add to numbers
        p.innerHTML = "Addition is " + ans.toString();  // Print the ans
    }   
}
function sub(){                     // Substraction function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +t1.value;
    var b:number = +t2.value;
    if(isNaN(a))
    {
        alert("First input is not a number");
    }
    else if(isNaN(b))
    {
        alert("Second input is not a number");
    }
    else{
        var ans:number = a - b;
        p.innerHTML += "<br>Substraction is " + ans.toString();
    }
}
function mul(){                     // Multiplication function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +t1.value;
    var b:number = +t2.value;
    if(isNaN(a))
    {
        alert("First input is not a number");
    }
    else if(isNaN(b))
    {
        alert("Second input is not a number");
    }
    else{
        var ans:number = a * b;
        p.innerHTML += "<br>Multiplivcation is " + ans.toString();
    }
}
function div(){                     // Division function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +t1.value;
    var b:number = +t2.value;
    if(isNaN(a))
    {
        alert("First input is not a number");
    }
    else if(isNaN(b))
    {
        alert("Second input is not a number");
    }
    else if(b == 0){
        alert("Not divide by zero");
    }
    else{
        var ans:number = a / b;
        p.innerHTML += "<br>Division is " + ans.toString();
    }
}
function sin1(){                    // Sin function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    if(isNaN(+t1.value)){
        alert("Input is not a number");
    }
    else{
        var a:number = Math.PI/180*parseFloat(t1.value);        // Convert value into radian
        var ans:number = Math.sin(a);
        p.innerHTML += "<br>Sin of input is" + ans.toString();
    }
}
function cos1(){                // Cos function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    if(isNaN(+t1.value)){
        alert("First Input is not a number");
    }
    else{
        var a:number = Math.PI/180*parseFloat(t1.value);
        var ans:number = Math.cos(a);
        p.innerHTML += "<br>Cos of input is" + ans.toString();
    }
}
function tan1(){                // Tan function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    if(isNaN(+t1.value)){
        alert("Input is not a number");
    }
    else if(+t1.value==90)      // Handle exception case for tan90 that is infinite
    {
        alert("Tan90 is undefinded");
    }
    else{
        var a:number = Math.PI/180*parseFloat(t1.value);
        var ans:number = Math.tan(a);
        p.innerHTML += "<br>Tan of input is" + ans.toString();
    }
}
function sroot(){           // Square root function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +t1.value;
    if(isNaN(a))
    {
        alert("Input is not a number");
    }
    else{
        var ans:number = Math.sqrt(a);
        p.innerHTML += "<br>Square root of input is" + ans.toString();
    }
}
function power(){           // power function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +t1.value;
    var b:number = +t2.value;
    if(isNaN(a))
    {
        alert("First input is not a number");
    }
    else if(isNaN(b))
    {
        alert("Second input is not a number");
    }
    else{
        var ans:number = Math.pow(a,b);
        p.innerHTML += "<br>Ans of Power is" + ans.toString();
    }
}