function cosx()
{
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");      // Get the input value
    let ans:HTMLInputElement = <HTMLInputElement>document.getElementById("ans");    // Get the output element
    var c:number = parseFloat(t1.value) + Math.cos(Math.PI/180*parseFloat(t1.value));   // Logic for ans=x+cosx
    ans.innerHTML = "Ans is " + c.toString();                                           // Print the output
}   