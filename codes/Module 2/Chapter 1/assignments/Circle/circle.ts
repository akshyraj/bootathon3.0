function area()             // Function for the calculate the area
{
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");      // Get the  input value
    let ans:HTMLInputElement = <HTMLInputElement>document.getElementById("ans");    // Get the output element
    var c:number = Math.PI*Math.pow(parseFloat(t1.value) ,2);                       // Logic for calculate the area= pi * t2 * t1
    ans.innerHTML = "Ans is " + c.toString();                                       // Print the output
}   