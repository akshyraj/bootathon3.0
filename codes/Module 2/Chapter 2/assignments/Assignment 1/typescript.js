function type1() {
    let t1 = document.getElementById("t1"); // Take  the input string "TYPESRIPT from the user"
    var p = document.getElementById("p"); // P tag for output
    if (t1.value == "TYPESCRIPT") // Check for the input is correct string or not
     {
        var a = t1.value.substring(4, 10); // Get output from the substring() method
        p.innerHTML += "<br>Substring function :" + a.toString(); // Display output
        var b = 1 + t1.value.indexOf("E"); // Get the position of "E" and add 1 because starting position in string is '0'
        p.innerHTML += "<br>Position of \'E\' is " + b;
    }
    else {
        alert("Enter the right string"); // Alert to user to write right string
    }
}
//# sourceMappingURL=typescript.js.map