function type1()
{
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");          // Take  the input string "Virtual Labs Bootathon 2020" from the user"
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");    // P tag for output
    if(t1.value == "Virtual Labs Bootathon 2020")               // Check for the input is correct string or not
    {
        var a:string = t1.value.toUpperCase();                  // Get output from the toUpperCase() method
        p.innerHTML += "<br>Upper case of string :" + a.toString();   // Display output
        a = t1.value.toLowerCase();                  // Get output from the toLowerCase() method
        p.innerHTML += "<br>Lower case of string :" + a.toString();   // Display output
        var b = t1.value.split(" ");                // Split the string by " "(space)
        p.innerHTML += "<br> Parts after the spliting"   //Display the array after the split the string
        p.innerHTML += "<br>First part : " + b[0];
        p.innerHTML += "<br>Second part : " + b[1];
        p.innerHTML += "<br>Third part : " +b[2];
        p.innerHTML += "<br>Fourth part : " +b[3];
    }
    else
    {
        alert("Enter the right string");                // Alert to user to write right string
    }
}